


$().ready(function(){
    $("#xa-background").attr('src', 'img/Cityscape%201.jpg');
    //menu search close

    $(".xa-body").click(function(){
        $(".xa-head").removeClass("xa-search-open");
        $(".xa-search-bar").val("");
    });

    $('body').flowtype({
        minFont : 10,
        maxFont : 15,
        minimum : 450,
        maximum : 1200
    });
    

    //menu search open
    $(".xa-icon:nth-child(3)").click(function(){
        $(".xa-head").addClass("xa-search-open");
    });


    //search submit
    $(".xa-search-button").click(function(){
        if($('.xa-search-bar').val()!='')
            $('.xa-search-form').submit();
        else
            $('.xa-search-bar').value=''
            });

    $(".xa-side-bar>ul>li").click(function(){
        var n=$(".xa-side-bar>ul>li").index(this);
        $(".xa-side-bar>ul>li").removeClass('xa-list-active');
        $(".xa-side-bar>ul>li:nth-child("+(n+1)+")").addClass('xa-list-active');
        n*=1.9;
        n=n+'em'
        $(".xa-list-select").animate({top:n},100,"linear");

    });

/*    
    (function($){
        $(window).load(function(){
            $(".content").mCustomScrollbar({
                scrollInertia:150,
                mouseWheel:true,
                autoDraggerLength:true,
                mouseWheelPixels: 110,
                advanced:{
                    updateOnBrowserResize:true,
                    updateOnContentResize: true,
                },
                contentTouchScroll:true, 
            });

            $(document).data({"keyboard-input":"enabled"});
            $("body").addClass("keyboard-input");

            $(document).keydown(function(e){
                if($(this).data("keyboard-input")==="enabled"){
                    var activeElem=$(".keyboard-input"),
                        activeElemPos=Math.abs($(".keyboard-input .mCSB_container").position().top),
                        pixelsToScroll=90;
                    if(e.which===38){ //scroll up
                        e.preventDefault();
                        if(pixelsToScroll>activeElemPos){
                            activeElem.mCustomScrollbar("scrollTo","top");
                        }else{
                            activeElem.mCustomScrollbar("scrollTo",(activeElemPos-pixelsToScroll),{scrollInertia:100,scrollEasing:"easeOutCirc"});
                        }
                    }else if(e.which===40){ //scroll down
                        e.preventDefault();
                        activeElem.mCustomScrollbar("scrollTo",(activeElemPos+pixelsToScroll),{scrollInertia:100,scrollEasing:"easeOutCirc"});
                    }
                }
            });

        });
    })(jQuery);

    /*
    $(window).resize(function(){
        $(".xa-about-recents").mCustomScrollbar("update");
    });
    */



    var x = $(".xa-body"),
        y = $(".xa-settings-panel"),
        z = $(".xa-head");

    $("#settings").click(function(){
        $("body").mCustomScrollbar("disable");
        x.css({opacity:0.1,'-webkit-transform':"scale(0)"});
        y.css({'-webkit-transform':"scale(1)"});
    });

    $("#xa-back").click(function(){
        y.css({'-webkit-transform':"scale(0)"});
        x.css({opacity:1,'-webkit-transform':"scale(1)"});
        $("body").mCustomScrollbar("update");
    });
});
